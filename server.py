import json

import redis
from flask import (
    Flask,
    abort,
    jsonify,
    redirect,
    render_template,
    request,
    session,
    url_for,
)

from config import (
    PLAID_COUNTRY_CODES,
    PLAID_PRODUCTS,
    REDIS_HOST,
    REDIS_PORT,
    SECRET_KEY,
    SERVER_PORT,
)
from etl import plaid_client, store_plaid_data, transactions_collection
from middleware import login_required

redis_client = redis.Redis(host=REDIS_HOST, port=REDIS_PORT)


app = Flask(__name__)
app.secret_key = SECRET_KEY
app.config.update(SESSION_TYPE="redis", SESSION_REDIS=redis_client)


@app.route("/", methods=["GET"])
@login_required
def index():
    transaction_data = transactions_collection.find_one(
        {"access_token": session["access_token"]}
    )

    transactions = None
    if transaction_data:
        transactions = transaction_data["transactions"]

    return render_template("index.html", transactions=json.dumps(transactions))


@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "GET":
        return render_template(
            "login.html",
            plaid_public_key=plaid_client.public_key,
            plaid_environment=plaid_client.environment,
            plaid_products=PLAID_PRODUCTS,
            plaid_country_codes=PLAID_COUNTRY_CODES,
        )

    public_token = request.form["public_token"]
    try:
        exchange_response = plaid_client.Item.public_token.exchange(public_token)
    except plaid.errors.PlaidError:
        app.logger.exception("Encountered an error while exchanging the token")
        abort(500)

    session["access_token"] = exchange_response["access_token"]
    return redirect(url_for("index"))


@app.route("/logout", methods=["POST"])
def logout():
    session.clear()
    return redirect(url_for("login"))


@app.route("/load-data", methods=["POST"])
@login_required
def load_data():
    app.logger.info("Loading data from Plaid")
    transactions = store_plaid_data(session["access_token"], app.logger)
    return jsonify({"message": "Loaded data", "transactions": transactions})


if __name__ == "__main__":
    app.run(port=SERVER_PORT)

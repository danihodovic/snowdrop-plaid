# Plaid - Snowdrop

An example repository on how to use the Plaid API.

### Requirements
- [docker](https://docs.docker.com/install/)
- [docker-compose](https://docs.docker.com/compose/install/)

### Installation

Clone the project
```bash
git clone https://gitlab.com/danihodovic/snowdrop-plaid.git
```

Add the credentials and variables. This should have been emailed to you.

The following environment variables are required:

- PLAID_CLIENT_ID
- PLAID_SECRET
- PLAID_PUBLIC_KEY
- PLAID_ENV=sandbox
- PLAID_PRODUCTS=transactions
- PLAID_COUNTRY_CODES=US,CA,GB,FR,ES

```bash
cd snowdrop-plaid
vim .env
# Add the credentials and close the file
```

Build and start the project
```bash
docker-compose build
docker-compose up -d
```

# Open your browser at localhost:5000

```bash
firefox localhost:5000
```

from functools import wraps

from flask import session, redirect, url_for, request

def login_required(f):
    @wraps(f)
    def fn(*args, **kwargs):
        access_token = session.get("access_token")
        if not access_token:
            return redirect(url_for("login", next=request.url))

        return f(*args, **kwargs)
    return fn

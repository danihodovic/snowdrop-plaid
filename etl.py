import copy
from datetime import datetime, timedelta

import plaid
import pymongo

from config import (
    MONGO_DB,
    MONGO_HOST,
    MONGO_PORT,
    PLAID_CLIENT_ID,
    PLAID_ENV,
    PLAID_PUBLIC_KEY,
    PLAID_SECRET,
)

mongo_client = pymongo.MongoClient(MONGO_HOST, MONGO_PORT)
db = mongo_client[MONGO_DB]

identity_collection = db.identity
transactions_collection = db.transactions
auth_collection = db.auth_collection

plaid_client = plaid.Client(
    client_id=PLAID_CLIENT_ID,
    secret=PLAID_SECRET,
    public_key=PLAID_PUBLIC_KEY,
    environment=PLAID_ENV,
    api_version="2019-05-29",
)


def store_plaid_data(access_token, logger):
    identity_response = plaid_client.Identity.get(access_token)
    auth_response = plaid_client.Auth.get(access_token)

    # Pull transactions for the last 30 days
    start_date = "{:%Y-%m-%d}".format(datetime.now() + timedelta(-30))
    end_date = "{:%Y-%m-%d}".format(datetime.now())
    transactions_response = plaid_client.Transactions.get(
        access_token, start_date, end_date
    )

    at = {"access_token": access_token}
    opts = {"upsert": True}
    try:
        identity_collection.update(
            at, copy.deepcopy({**identity_response, **at}), **opts
        )
        auth_collection.update(at, copy.deepcopy({**auth_response, **at}), **opts)
        transactions_collection.update(
            at, copy.deepcopy({**transactions_response, **at}), **opts
        )
        return transactions_response["transactions"]
    except:
        logger.exception("Error")

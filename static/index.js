/* global $ */

window.addEventListener('DOMContentLoaded', () => {
  const loadDataBtn = $('.load-data-btn')

  loadDataBtn.on('click', async () => {
    loadDataBtn.hide();
    $('.spinner-border').removeClass('d-none');
    const data = await $.post({url: '/load-data'});
    renderTable(data.transactions);
    $('.spinner-border').addClass('d-none');
    $('.transactions-container').removeClass('d-none');
  });

  if (window.transactions) {
    loadDataBtn.hide();
    renderTable(window.transactions);
    $('.transactions-container').removeClass('d-none');
  }

  $('.log-out-btn').on('click', () => {
    $('.log-out-form').submit();
  });
});

function renderTable(transactions) {
  const categoryIconMap = {
    'Travel': 'fa-plane',
    'Food and Drink': 'fa-utensils',
    'Payment': 'fa-credit-card',
    'Shops': 'fa-shopping-cart',
    'Transfer': 'fa-random',
    'Recreation': 'fa-futbol',
  };

  let rows = '';
  for (const transaction of transactions) {
    const icon = categoryIconMap[transaction.category[0]] || 'fa-question';
    rows += `
      <tr>
        <th scope="row">${transaction.date}</th>
        <td>${transaction.name}</td>
        <td><i class="fas ${icon} mr-3"></i>${transaction.category[0]}</td>
        <td>$${transaction.amount}</td>
      </tr>
    `
  }

  const table = `
<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">Date</th>
      <th scope="col">Description</th>
      <th scope="col">Category</th>
      <th scope="col">Amount</th>
    </tr>
  </thead>
  <tbody>
    ${rows}
  </tbody>
</table>
`;

  $('.transactions-table').html(table);
}

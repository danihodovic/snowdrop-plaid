window.addEventListener('DOMContentLoaded', event => {
  const handler = Plaid.create({
    apiVersion: 'v2',
    clientName: 'Snowdrop',
    env: window.PLAID_ENVIRONMENT,
    product: window.PLAID_PRODUCTS,
    key: window.PLAID_PUBLIC_KEY,
    countryCodes: window.PLAID_COUNTRY_CODES,
    onSuccess(public_token) {
      $('#public-token').val(public_token);
      $('#login-form').submit();
    },
    error(err) {
      console.log('got err', err);
    }
  });

  $('#link-btn').on('click', function(e) {
    handler.open();
  });
});

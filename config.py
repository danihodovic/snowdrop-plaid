import os

SECRET_KEY = os.getenv("SECRET_KEY", "very-secret")
SERVER_PORT = os.getenv("SERVER_PORT", 5000)

# Plaid
PLAID_CLIENT_ID = os.getenv("PLAID_CLIENT_ID")
PLAID_SECRET = os.getenv("PLAID_SECRET")
PLAID_PUBLIC_KEY = os.getenv("PLAID_PUBLIC_KEY")
PLAID_ENV = os.getenv("PLAID_ENV", "sandbox")
PLAID_PRODUCTS = os.getenv("PLAID_PRODUCTS", "transactions")
PLAID_COUNTRY_CODES = os.getenv("PLAID_COUNTRY_CODES", "US,CA,GB,FR,ES")

# Mongo
MONGO_HOST = os.getenv("MONGO_HOST", "localhost")
MONGO_PORT = os.getenv("MONGO_PORT", "27017")
MONGO_PORT = int(MONGO_PORT)
MONGO_DB = os.getenv("MONGO_DB", "snowdrop")

# Redis
REDIS_HOST = os.getenv("REDIS_HOST", "localhost")
REDIS_PORT = os.getenv("REDIS_PORT", "6379")
REDIS_PORT = int(MONGO_PORT)

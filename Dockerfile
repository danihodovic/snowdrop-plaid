FROM python:3.7

RUN apt-get update && apt install -y locales \
        && apt-get clean \
        && rm -rf /var/lib/apt/lists/*

RUN echo "LC_ALL=en_US.UTF-8" >> /etc/environment && \
	echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen && \
	echo "LANG=en_US.UTF-8" > /etc/locale.conf && \
	locale-gen en_US.UTF-8

WORKDIR /app/

COPY ./requirements.txt /app/
RUN pip install -r requirements.txt

ENV PYTHONBREAKPOINT pudb.set_trace

COPY . /app/

CMD ["flask", "run", "--host=0.0.0.0"]
